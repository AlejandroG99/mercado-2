package mercado.test.testGestionLecturas;

import static org.junit.Assert.assertEquals;

import org.junit.Test;

import mercado.gestionLecturas.Lectura;
import mercado.gestionLecturas.MedicionExistenteException;
import mercado.gestionLecturas.MedicionInexistenteException;
import mercado.gestionLecturas.MedidorLuz;

public class MedidorLuzTest {

    @Test
    public void agregarLectura() throws MedicionExistenteException{
        MedidorLuz medidor1 = new MedidorLuz();
        Lectura lectura1 = new Lectura(10000);
        Lectura lectura2 = new Lectura(20000);
        medidor1.agregarLectura(lectura1);
        medidor1.agregarLectura(lectura2);
        assertEquals(2, medidor1.getListaLecturas().size());
    }

    @Test
    public void leerLectura() throws MedicionExistenteException, MedicionInexistenteException{
        MedidorLuz medidor1 = new MedidorLuz();
        Lectura lectura1 = new Lectura(10000);
        Lectura lectura2 = new Lectura(20000);
        medidor1.agregarLectura(lectura1);
        medidor1.agregarLectura(lectura2);
        assertEquals(lectura1, medidor1.getLectura(10000));
    }

    @Test
    public void modificarContratoLectura() throws MedicionExistenteException, MedicionInexistenteException{
        MedidorLuz medidor1 = new MedidorLuz();
        Lectura lectura1 = new Lectura(10000);
        Lectura lectura2 = new Lectura(20000);
        medidor1.agregarLectura(lectura1);
        medidor1.agregarLectura(lectura2);
        medidor1.modificarLectura(10000, new Lectura(3000));
        assertEquals(Integer.valueOf(3000), medidor1.getLectura(3000).getLectura());
    }

    @Test
    public void eliminarContratoLectura() throws MedicionExistenteException, MedicionInexistenteException{
        MedidorLuz medidor1 = new MedidorLuz();
        Lectura lectura1 = new Lectura(10000);
        Lectura lectura2 = new Lectura(20000);
        medidor1.agregarLectura(lectura1);
        medidor1.agregarLectura(lectura2);
        medidor1.eliminarLectura(10000);
        assertEquals(1, medidor1.getListaLecturas().size());
    }

    @Test(expected = MedicionExistenteException.class)
    public void agregarLecturaException() throws MedicionExistenteException{
        MedidorLuz medidor1 = new MedidorLuz();
        Lectura lectura1 = new Lectura(10000);
        medidor1.agregarLectura(lectura1);
        medidor1.agregarLectura(lectura1);
    }

    @Test(expected = MedicionInexistenteException.class)
    public void leerLecturaException() throws MedicionExistenteException, MedicionInexistenteException{
        MedidorLuz medidor1 = new MedidorLuz();
        medidor1.getLectura(10000);
    }
}