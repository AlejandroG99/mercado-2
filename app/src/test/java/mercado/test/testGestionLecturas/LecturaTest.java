package mercado.test.testGestionLecturas;

import static org.junit.Assert.assertEquals;

import org.junit.Test;
import mercado.gestionLecturas.Lectura;

public class LecturaTest {

    @Test
    public void testObtenerLectura(){
        Lectura lectura1 = new Lectura(2000);
        assertEquals(Integer.valueOf(2000), lectura1.getLectura());
    }

    @Test
    public void testCompararLecturasIguales(){
        Lectura lectura1 = new Lectura(2000);
        Lectura lectura2 = new Lectura(2000);
        assertEquals(true, lectura1.equals(lectura2));
    }
        
    @Test
    public void testCompararLecturasDistintos(){
        Lectura lectura1 = new Lectura(2000);
        Lectura lectura3 = new Lectura(6000);
        assertEquals(false, lectura1.equals(lectura3));
    }
}
