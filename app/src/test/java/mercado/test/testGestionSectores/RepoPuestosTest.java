package mercado.test.testGestionSectores;

import static org.junit.Assert.assertEquals;

import org.junit.Test;

import mercado.gestionLecturas.Lectura;
import mercado.gestionLecturas.MedicionExistenteException;
import mercado.gestionLecturas.MedidorLuz;
import mercado.gestionSectores.Puesto;
import mercado.gestionSectores.PuestoExistenteException;
import mercado.gestionSectores.PuestoInexistenteException;
import mercado.gestionSectores.RepoPuestos;

public class RepoPuestosTest {

    @Test
    public void agregarPuesto() throws PuestoExistenteException, MedicionExistenteException{
        Lectura lectura1 = new Lectura(2000);
        MedidorLuz medidor = new MedidorLuz();
        medidor.agregarLectura(lectura1);
        Puesto puesto1 = new Puesto(1, "Frigorifico", 300, 400, true, false, false, medidor);
        Puesto puesto2 = new Puesto(2, "Verduleria", 200, 300, false, false, false, medidor);
        RepoPuestos listaPuestos = new RepoPuestos();
        listaPuestos.agregarPuesto(puesto1);
        listaPuestos.agregarPuesto(puesto2);
        assertEquals(2, listaPuestos.getListaPuestos().size());
    }

    @Test
    public void leerPuesto() throws PuestoExistenteException, PuestoInexistenteException, MedicionExistenteException{
        Lectura lectura1 = new Lectura(2000);
        MedidorLuz medidor = new MedidorLuz();
        medidor.agregarLectura(lectura1);
        Puesto puesto1 = new Puesto(1, "Frigorifico", 300, 400, true, false, false, medidor);        RepoPuestos listaPuestos = new RepoPuestos();
        listaPuestos.agregarPuesto(puesto1);
        assertEquals(puesto1, listaPuestos.getPuesto(1));
    }

    @Test
    public void modificarContratoPuesto() throws PuestoExistenteException, PuestoInexistenteException, MedicionExistenteException{
        Lectura lectura1 = new Lectura(2000);
        MedidorLuz medidor = new MedidorLuz();
        medidor.agregarLectura(lectura1);
        Puesto puesto1 = new Puesto(1, "Frigorifico", 300, 400, true, false, false, medidor);
        RepoPuestos listaPuestos = new RepoPuestos();
        listaPuestos.agregarPuesto(puesto1);
        listaPuestos.modificarPuesto(1, new Puesto(7, "Kiosco", 200, 300, false, false, false, medidor));
        assertEquals("Kiosco", listaPuestos.getPuesto(7).getTipoPuesto());
    }

    @Test
    public void eliminarContratoPuesto() throws PuestoExistenteException, PuestoInexistenteException, MedicionExistenteException{
        Lectura lectura1 = new Lectura(2000);
        MedidorLuz medidor = new MedidorLuz();
        medidor.agregarLectura(lectura1);
        Puesto puesto1 = new Puesto(1, "Frigorifico", 300, 400, true, false, false, medidor);
        Puesto puesto2 = new Puesto(2, "Verduleria", 200, 300, false, false, false, medidor);
        RepoPuestos listaPuestos = new RepoPuestos();
        listaPuestos.agregarPuesto(puesto1);
        listaPuestos.agregarPuesto(puesto2);
        listaPuestos.eliminarPuesto(1);
        assertEquals(1, listaPuestos.getListaPuestos().size());
    }
    @Test (expected = PuestoExistenteException.class)
    public void agregarPuestoException() throws PuestoExistenteException, MedicionExistenteException{
        Lectura lectura1 = new Lectura(2000);
        MedidorLuz medidor = new MedidorLuz();
        medidor.agregarLectura(lectura1);
        Puesto puesto1 = new Puesto(1, "Frigorifico", 300, 400, true, false, false, medidor);
        RepoPuestos listaPuestos = new RepoPuestos();
        listaPuestos.agregarPuesto(puesto1);
        listaPuestos.agregarPuesto(puesto1);
    }

    @Test (expected = PuestoInexistenteException.class)
    public void leerPuestoException() throws PuestoExistenteException, PuestoInexistenteException, MedicionExistenteException{
        RepoPuestos listaPuestos = new RepoPuestos();
        listaPuestos.getPuesto(1);
    }
}