package mercado.test.testGestionSectores;

import org.junit.Test;

import mercado.gestionLecturas.Lectura;
import mercado.gestionLecturas.MedicionExistenteException;
import mercado.gestionLecturas.MedidorLuz;
import mercado.gestionSectores.Puesto;
import mercado.gestionSectores.PuestoExistenteException;
import mercado.gestionSectores.RepoPuestos;
import mercado.gestionSectores.Sector;
import mercado.gestionSectores.SectorExistenteException;

import static org.junit.Assert.assertEquals;

public class SectorTest{

    @Test
    public void testObtenerNumeroPuestos() throws PuestoExistenteException, MedicionExistenteException{
        Lectura lectura1 = new Lectura(2000);
        MedidorLuz medidor = new MedidorLuz();
        medidor.agregarLectura(lectura1);
        Puesto puesto1 = new Puesto(1, "Frigorifico", 300, 400, true, false, false, medidor);
        Puesto puesto2 = new Puesto(2, "Verduleria", 200, 300, false, false, false, medidor);
        RepoPuestos listaPuestos = new RepoPuestos();
        listaPuestos.agregarPuesto(puesto1);
        listaPuestos.agregarPuesto(puesto2);
        Sector sector = new Sector(2, "Norte");
        assertEquals(Integer.valueOf(2), sector.getNumeroPuestos());
    }

    @Test
    public void testObtenerNombreSector() throws PuestoExistenteException, SectorExistenteException, MedicionExistenteException{
        Lectura lectura1 = new Lectura(2000);
        MedidorLuz medidor = new MedidorLuz();
        medidor.agregarLectura(lectura1);
        Puesto puesto1 = new Puesto(1, "Frigorifico", 300, 400, true, false, false, medidor);
        Puesto puesto2 = new Puesto(2, "Verduleria", 200, 300, false, false, false, medidor);
        RepoPuestos listaPuestos = new RepoPuestos();
        listaPuestos.agregarPuesto(puesto1);
        listaPuestos.agregarPuesto(puesto2);
        Sector sector = new Sector(2, "Norte");
        assertEquals("Norte", sector.getNombreSector());
    }

    @Test
    public void testObtenerRepoPuestos() throws PuestoExistenteException, SectorExistenteException, MedicionExistenteException{
        Lectura lectura1 = new Lectura(2000);
        MedidorLuz medidor = new MedidorLuz();
        medidor.agregarLectura(lectura1);
        Puesto puesto1 = new Puesto(1, "Frigorifico", 300, 400, true, false, false, medidor);
        Puesto puesto2 = new Puesto(2, "Verduleria", 200, 300, false, false, false, medidor);
        RepoPuestos listaPuestos = new RepoPuestos();
        listaPuestos.agregarPuesto(puesto1);
        listaPuestos.agregarPuesto(puesto2);
        Sector sector = new Sector(2, "Norte");
        sector.setRepoPuestos(listaPuestos);
        assertEquals(listaPuestos, sector.getRepoPuestos());
    }

    @Test
    public void testCompararSectoresIguales() throws PuestoExistenteException, SectorExistenteException, MedicionExistenteException{
        Lectura lectura1 = new Lectura(2000);
        MedidorLuz medidor = new MedidorLuz();
        medidor.agregarLectura(lectura1);
        Puesto puesto1 = new Puesto(1, "Frigorifico", 300, 400, true, false, false, medidor);
        Puesto puesto2 = new Puesto(2, "Verduleria", 200, 300, false, false, false, medidor);
        RepoPuestos listaPuestos = new RepoPuestos();
        listaPuestos.agregarPuesto(puesto1);
        listaPuestos.agregarPuesto(puesto2);
        Sector sector1 = new Sector(2, "Norte");
        Sector sector2 = new Sector(2, "Norte");
        sector1.setRepoPuestos(listaPuestos);
        sector2.setRepoPuestos(listaPuestos);
        assertEquals(true, sector1.equals(sector2));
    }

    @Test
    public void testCompararSectoresDistintos()throws PuestoExistenteException, SectorExistenteException, MedicionExistenteException{
        Lectura lectura1 = new Lectura(2000);
        MedidorLuz medidor = new MedidorLuz();
        medidor.agregarLectura(lectura1);
        Puesto puesto1 = new Puesto(1, "Frigorifico", 300, 400, true, false, false, medidor);
        Puesto puesto2 = new Puesto(2, "Verduleria", 200, 300, false, false, false, medidor);
        RepoPuestos listaPuestos1 = new RepoPuestos();
        RepoPuestos listaPuestos2 = new RepoPuestos();
        listaPuestos1.agregarPuesto(puesto1);
        listaPuestos1.agregarPuesto(puesto2);
        listaPuestos2.agregarPuesto(puesto2);
        listaPuestos2.agregarPuesto(puesto1);
        Sector sector1 = new Sector(2, "Norte");
        Sector sector2 = new Sector(2, "Norte");
        sector1.setRepoPuestos(listaPuestos1);
        sector2.setRepoPuestos(listaPuestos2);
        assertEquals(false, sector1.equals(sector2));
    }
}