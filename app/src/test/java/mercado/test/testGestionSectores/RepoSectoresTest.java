package mercado.test.testGestionSectores;

import static org.junit.Assert.assertEquals;

import org.junit.Test;

import mercado.gestionLecturas.Lectura;
import mercado.gestionLecturas.MedicionExistenteException;
import mercado.gestionLecturas.MedidorLuz;
import mercado.gestionSectores.Sector;
import mercado.gestionSectores.SectorExistenteException;
import mercado.gestionSectores.SectorInexistenteException;
import mercado.gestionSectores.Puesto;
import mercado.gestionSectores.PuestoExistenteException;
import mercado.gestionSectores.RepoPuestos;
import mercado.gestionSectores.RepoSectores;

public class RepoSectoresTest {

    @Test
    public void agregarSector() throws SectorExistenteException, PuestoExistenteException, MedicionExistenteException{
        Lectura lectura1 = new Lectura(2000);
        MedidorLuz medidor = new MedidorLuz();
        medidor.agregarLectura(lectura1);
        Puesto puesto1 = new Puesto(1, "Frigorifico", 300, 400, true, false, false, medidor);
        Puesto puesto2 = new Puesto(2, "Verduleria", 200, 300, false, false, false, medidor);
        RepoPuestos listaPuestos1 = new RepoPuestos();
        RepoPuestos listaPuestos2 = new RepoPuestos();
        listaPuestos1.agregarPuesto(puesto1);
        listaPuestos1.agregarPuesto(puesto2);
        listaPuestos2.agregarPuesto(puesto2);
        listaPuestos2.agregarPuesto(puesto1);
        Sector sector1 = new Sector(2, "Norte");
        Sector sector2 = new Sector(2, "Sur");
        sector1.setListaPuestos(listaPuestos1);
        sector2.setListaPuestos(listaPuestos2);
        RepoSectores listaSectores = new RepoSectores();
        listaSectores.agregarSector(sector1);
        listaSectores.agregarSector(sector2);
        
        assertEquals(2, listaSectores.getlistaSectores().size());
    }

    @Test
    public void leerSector() throws SectorExistenteException, SectorInexistenteException, PuestoExistenteException, MedicionExistenteException{
        Lectura lectura1 = new Lectura(2000);
        MedidorLuz medidor = new MedidorLuz();
        medidor.agregarLectura(lectura1);
        Puesto puesto1 = new Puesto(1, "Frigorifico", 300, 400, true, false, false, medidor);
        Puesto puesto2 = new Puesto(2, "Verduleria", 200, 300, false, false, false, medidor);
        RepoPuestos listaPuestos1 = new RepoPuestos();
        RepoPuestos listaPuestos2 = new RepoPuestos();
        listaPuestos1.agregarPuesto(puesto1);
        listaPuestos1.agregarPuesto(puesto2);
        listaPuestos2.agregarPuesto(puesto2);
        listaPuestos2.agregarPuesto(puesto1);
        Sector sector1 = new Sector(2, "Norte");
        Sector sector2 = new Sector(2, "Sur");
        sector1.setListaPuestos(listaPuestos1);
        sector2.setListaPuestos(listaPuestos2);
        RepoSectores listaSectores = new RepoSectores();
        listaSectores.agregarSector(sector1);
        listaSectores.agregarSector(sector2);
        
        assertEquals(sector1, listaSectores.getSector("Norte"));
    }

    @Test
    public void modificarSector() throws SectorExistenteException, SectorInexistenteException, PuestoExistenteException, MedicionExistenteException{
        Lectura lectura1 = new Lectura(2000);
        MedidorLuz medidor = new MedidorLuz();
        medidor.agregarLectura(lectura1);
        Puesto puesto1 = new Puesto(1, "Frigorifico", 300, 400, true, false, false, medidor);
        Puesto puesto2 = new Puesto(2, "Verduleria", 200, 300, false, false, false, medidor);
        RepoPuestos listaPuestos1 = new RepoPuestos();
        RepoPuestos listaPuestos2 = new RepoPuestos();
        listaPuestos1.agregarPuesto(puesto1);
        listaPuestos1.agregarPuesto(puesto2);
        listaPuestos2.agregarPuesto(puesto2);
        listaPuestos2.agregarPuesto(puesto1);
        Sector sector1 = new Sector(2, "Norte");
        Sector sector2 = new Sector(2, "Sur");
        sector1.setListaPuestos(listaPuestos1);
        sector2.setListaPuestos(listaPuestos2);
        RepoSectores listaSectores = new RepoSectores();
        listaSectores.agregarSector(sector1);
        listaSectores.agregarSector(sector2);
        Sector sector3 = new Sector(2, "Este");
        sector3.setListaPuestos(listaPuestos1);
        listaSectores.modificarSector("Norte", sector3);
        
        assertEquals("Este", listaSectores.getSector("Este").getNombreSector());
    }

    @Test
    public void eliminarSector() throws SectorExistenteException, SectorInexistenteException, PuestoExistenteException, MedicionExistenteException{
        Lectura lectura1 = new Lectura(2000);
        MedidorLuz medidor = new MedidorLuz();
        medidor.agregarLectura(lectura1);
        Puesto puesto1 = new Puesto(1, "Frigorifico", 300, 400, true, false, false, medidor);
        Puesto puesto2 = new Puesto(2, "Verduleria", 200, 300, false, false, false, medidor);
        RepoPuestos listaPuestos1 = new RepoPuestos();
        RepoPuestos listaPuestos2 = new RepoPuestos();
        listaPuestos1.agregarPuesto(puesto1);
        listaPuestos1.agregarPuesto(puesto2);
        listaPuestos2.agregarPuesto(puesto2);
        listaPuestos2.agregarPuesto(puesto1);
        Sector sector1 = new Sector(2, "Norte");
        Sector sector2 = new Sector(2, "Sur");
        sector1.setListaPuestos(listaPuestos1);
        sector2.setListaPuestos(listaPuestos2);
        RepoSectores listaSectores = new RepoSectores();
        listaSectores.agregarSector(sector1);
        listaSectores.agregarSector(sector2);
        listaSectores.eliminarSector("Sur");
        
        assertEquals(1, listaSectores.getlistaSectores().size());
    }

    @Test (expected = SectorExistenteException.class)
    public void agregarSectorException() throws SectorExistenteException, PuestoExistenteException, MedicionExistenteException{
        Lectura lectura1 = new Lectura(2000);
        MedidorLuz medidor = new MedidorLuz();
        medidor.agregarLectura(lectura1);
        Puesto puesto1 = new Puesto(1, "Frigorifico", 300, 400, true, false, false, medidor);
        Puesto puesto2 = new Puesto(2, "Verduleria", 200, 300, false, false, false, medidor);
        RepoPuestos listaPuestos1 = new RepoPuestos();
        listaPuestos1.agregarPuesto(puesto1);
        listaPuestos1.agregarPuesto(puesto2);
        Sector sector1 = new Sector(2, "Norte");
        sector1.setListaPuestos(listaPuestos1);
        RepoSectores listaSectores = new RepoSectores();
        listaSectores.agregarSector(sector1);
        listaSectores.agregarSector(sector1);
    }

    @Test (expected = SectorInexistenteException.class)
    public void leerSectorException() throws SectorExistenteException, SectorInexistenteException, PuestoExistenteException, MedicionExistenteException{
        RepoSectores listaSectores = new RepoSectores();
        listaSectores.getSector("Norte");
    }
}