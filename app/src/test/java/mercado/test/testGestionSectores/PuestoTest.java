package mercado.test.testGestionSectores; 

import static org.junit.Assert.assertEquals;

import org.junit.Test;

import mercado.gestionLecturas.Lectura;
import mercado.gestionLecturas.MedicionExistenteException;
import mercado.gestionLecturas.MedidorLuz;
import mercado.gestionSectores.Puesto;

public class PuestoTest {

    @Test
    public void testObtenerNumeroPuesto() throws MedicionExistenteException{
        Lectura lectura1 = new Lectura(2000);
        MedidorLuz medidor = new MedidorLuz();
        medidor.agregarLectura(lectura1);
        Puesto puesto1 = new Puesto(1, "Frigorifico", 300, 400, true, false, false, medidor);
        assertEquals(Integer.valueOf(1), puesto1.getNumeroPuesto());
    }

    @Test
    public void testObtenerTipoPuesto() throws MedicionExistenteException{
        Lectura lectura1 = new Lectura(2000);
        MedidorLuz medidor = new MedidorLuz();
        medidor.agregarLectura(lectura1);
        Puesto puesto1 = new Puesto(1, "Frigorifico", 300, 400, true, false, false, medidor);

        assertEquals("Frigorifico", puesto1.getTipoPuesto());
    }

    @Test
    public void testObtenerAncho() throws MedicionExistenteException{
        Lectura lectura1 = new Lectura(2000);
        MedidorLuz medidor = new MedidorLuz();
        medidor.agregarLectura(lectura1);
        Puesto puesto1 = new Puesto(1, "Frigorifico", 300, 400, true, false, false, medidor);

        assertEquals(Integer.valueOf(300), puesto1.getAncho());
    }

    @Test
    public void testObtenerLargo() throws MedicionExistenteException{
        Lectura lectura1 = new Lectura(2000);
        MedidorLuz medidor = new MedidorLuz();
        medidor.agregarLectura(lectura1);
        Puesto puesto1 = new Puesto(1, "Frigorifico", 300, 400, true, false, false, medidor);

        assertEquals(Integer.valueOf(400), puesto1.getLargo());
    }

    @Test
    public void testObtenerSiTieneTecho() throws MedicionExistenteException{
        Lectura lectura1 = new Lectura(2000);
        MedidorLuz medidor = new MedidorLuz();
        medidor.agregarLectura(lectura1);
        Puesto puesto1 = new Puesto(1, "Frigorifico", 300, 400, true, false, false, medidor);

        assertEquals(true, puesto1.getConTecho());
    }

    @Test
    public void testObtenerSiTieneCamaraRefrigerante() throws MedicionExistenteException{
        Lectura lectura1 = new Lectura(2000);
        MedidorLuz medidor = new MedidorLuz();
        medidor.agregarLectura(lectura1);
        Puesto puesto1 = new Puesto(1, "Frigorifico", 300, 400, true, false, false, medidor);

        assertEquals(false, puesto1.getConCamaraRefrigerante());
    }

    @Test
    public void testObtenerDisponibilidad() throws MedicionExistenteException{
        Lectura lectura1 = new Lectura(2000);
        MedidorLuz medidor = new MedidorLuz();
        medidor.agregarLectura(lectura1);
        Puesto puesto1 = new Puesto(1, "Frigorifico", 300, 400, true, false, false, medidor);

        assertEquals(false, puesto1.getDisponibilidad());
    }

    @Test
    public void testObtenerMedidorLuz() throws MedicionExistenteException{
        Lectura lectura1 = new Lectura(2000);
        MedidorLuz medidor = new MedidorLuz();
        medidor.agregarLectura(lectura1);
        Puesto puesto1 = new Puesto(1, "Frigorifico", 300, 400, true, false, false, medidor);

        assertEquals(medidor, puesto1.getMedidor());
    }

    @Test
    public void testCompararPuestosIguales() throws MedicionExistenteException{
        Lectura lectura1 = new Lectura(2000);
        MedidorLuz medidor = new MedidorLuz();
        medidor.agregarLectura(lectura1);
        Puesto puesto1 = new Puesto(1, "Frigorifico", 300, 400, true, false, false, medidor);
        Puesto puesto2 = new Puesto(1, "Frigorifico", 300, 400, true, false, false, medidor);

        assertEquals(true, puesto1.equals(puesto2));
    }

    @Test
    public void testCompararPuestosDistintos() throws MedicionExistenteException{
        Lectura lectura1 = new Lectura(2000);
        MedidorLuz medidor = new MedidorLuz();
        medidor.agregarLectura(lectura1);
        Puesto puesto1 = new Puesto(1, "Frigorifico", 300, 400, true, false, false, medidor);
        Puesto puesto3 = new Puesto(2, "Verduleria", 200, 300, false, false, false, medidor);

        assertEquals(false, puesto1.equals(puesto3));
    }
}
