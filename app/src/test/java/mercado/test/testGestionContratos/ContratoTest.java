package mercado.test.testGestionContratos; 

import static org.junit.Assert.assertEquals;

import org.junit.Test;

import mercado.gestionClientes.Cliente;
import mercado.gestionClientes.Quintero;
import mercado.gestionContratos.Contrato;

public class ContratoTest {

    @Test
    public void testObtenerClienteContrato(){
        Cliente quintero1 = new Quintero("27-99999991-1", "Pedro Sanchez 13", "quintero@gmail.com", "3834951245", "Gustavo", "Martinez");
        Contrato contrato = new Contrato(quintero1, "2-4-2000", "1-1-2001", 20000, 58, "Juan Carlos", 21454);
        assertEquals(quintero1, contrato.getClienteAlquilante());
    }

    @Test
    public void testObtenerFechaInicioContrato(){
        Cliente quintero1 = new Quintero("27-99999991-1", "Pedro Sanchez 13", "quintero@gmail.com", "3834951245", "Gustavo", "Martinez");
        Contrato contrato = new Contrato(quintero1, "2-4-2000", "1-1-2001", 20000, 58, "Juan Carlos", 21454);
        assertEquals("2-4-2000", contrato.getInicio());
    }

    @Test
    public void testObtenerFechaFinContrato(){
        Cliente quintero1 = new Quintero("27-99999991-1", "Pedro Sanchez 13", "quintero@gmail.com", "3834951245", "Gustavo", "Martinez");
        Contrato contrato = new Contrato(quintero1, "2-4-2000", "1-1-2001", 20000, 58, "Juan Carlos", 21454);
        assertEquals("1-1-2001", contrato.getFin());
    }

    @Test
    public void testObtenerMontoMensualContrato() {
        Cliente quintero1 = new Quintero("27-99999991-1", "Pedro Sanchez 13", "quintero@gmail.com", "3834951245", "Gustavo", "Martinez");
        Contrato contrato = new Contrato(quintero1, "2-4-2000", "1-1-2001", 20000, 58, "Juan Carlos", 21454);
        assertEquals(Integer.valueOf(20000), Integer.valueOf(contrato.getMontoMensual()));
    }

    @Test
    public void testObtenerIdContratoContrato() {
        Cliente quintero1 = new Quintero("27-99999991-1", "Pedro Sanchez 13", "quintero@gmail.com", "3834951245", "Gustavo", "Martinez");
        Contrato contrato = new Contrato(quintero1, "2-4-2000", "1-1-2001", 20000, 58, "Juan Carlos", 21454);
        assertEquals(Integer.valueOf(58), Integer.valueOf(contrato.getNumeroContrato()));
    }

    @Test
    public void testObtenerResponsableContrato(){
        Cliente quintero1 = new Quintero("27-99999991-1", "Pedro Sanchez 13", "quintero@gmail.com", "3834951245", "Gustavo", "Martinez");
        Contrato contrato = new Contrato(quintero1, "2-4-2000", "1-1-2001", 20000, 58, "Juan Carlos", 21454);
        assertEquals("Juan Carlos", contrato.getResponsableMercado()); 
    }

    @Test
    public void testObtenerUltimaMedicionContrato(){
        Cliente quintero1 = new Quintero("27-99999991-1", "Pedro Sanchez 13", "quintero@gmail.com", "3834951245", "Gustavo", "Martinez");
        Contrato contrato = new Contrato(quintero1, "2-4-2000", "1-1-2001", 20000, 58, "Juan Carlos", 21454);
        assertEquals(Integer.valueOf(21454), contrato.getUltimaMedicionLuz()); 
    }
    //-
    @Test
    public void testCompararContratosIguales(){
        Cliente quintero1 = new Quintero("27-99999991-1", "Pedro Sanchez 13", "quintero@gmail.com", "3834951245", "Gustavo", "Martinez");
        Contrato contrato1 = new Contrato(quintero1, "2-4-2000", "1-1-2001", 20000, 58, "Juan Carlos", 21454);
        Contrato contrato2 = new Contrato(quintero1, "2-4-2000", "1-1-2001", 20000, 58, "Juan Carlos", 21454);
        assertEquals(true, contrato1.equals(contrato2));
    }
        
    @Test
    public void testCompararContratosDistintos(){
        Cliente quintero1 = new Quintero("27-99999991-1", "Pedro Sanchez 13", "quintero@gmail.com", "3834951245", "Gustavo", "Martinez");
        Contrato contrato1 = new Contrato(quintero1, "2-4-2000", "1-1-2001", 20000, 58, "Juan Carlos", 21454);
        Cliente quintero2 = new Quintero("27-99639991-1", "Pedro Castro 47", "otroquino@gmail.com", "3834598778", "Federico", "Carrizo");
        Contrato contrato2 = new Contrato(quintero2, "8-2-1998", "5-4-2004", 25000, 78, "Juan Carlos", 62541);
        assertEquals(false, contrato1.equals(contrato2));
    }
}
