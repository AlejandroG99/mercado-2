package mercado.test.testGestionContratos;

import static org.junit.Assert.assertEquals;

import org.junit.Test;

import mercado.gestionClientes.Cliente;
import mercado.gestionClientes.Quintero;
import mercado.gestionContratos.Contrato;
import mercado.gestionContratos.ContratoExistenteException;
import mercado.gestionContratos.ContratoInexistenteException;
import mercado.gestionContratos.RepoContratos;

public class RepoContratosTest {
    
    @Test
    public void AgregarContrato() throws ContratoExistenteException{
        RepoContratos listaContratos = new RepoContratos();
        Cliente quintero1 = new Quintero("27-99999991-1", "Pedro Sanchez 13", "quintero@gmail.com", "3834951245", "Gustavo", "Martinez");
        Contrato contrato1 = new Contrato(quintero1, "2-4-2000", "1-1-2001", 20000, 58, "Juan Carlos", 21454);
        Contrato contrato2 = new Contrato(quintero1, "4-6-1997", "12-5-2005", 8900, 65, "Mario", 435);
        listaContratos.agregarContrato(contrato1);
        listaContratos.agregarContrato(contrato2);
        assertEquals(2, listaContratos.getListaContratos().size());
    }

    @Test
    public void leerContrato() throws ContratoExistenteException, ContratoInexistenteException{
        RepoContratos listaContratos = new RepoContratos();
        Cliente quintero1 = new Quintero("27-99999991-1", "Pedro Sanchez 13", "quintero@gmail.com", "3834951245", "Gustavo", "Martinez");
        Contrato contrato1 = new Contrato(quintero1, "2-4-2000", "1-1-2001", 20000, 58, "Juan Carlos", 21454);
        Contrato contrato2 = new Contrato(quintero1, "4-6-1997", "12-5-2005", 8900, 65, "Mario", 435);
        listaContratos.agregarContrato(contrato1);
        listaContratos.agregarContrato(contrato2);
        assertEquals(contrato1, listaContratos.getContrato(58));
    }

    @Test
    public void modificarContrato() throws ContratoExistenteException, ContratoInexistenteException{
        RepoContratos listaContratos = new RepoContratos();
        Cliente quintero1 = new Quintero("27-99999991-1", "Pedro Sanchez 13", "quintero@gmail.com", "3834951245", "Gustavo", "Martinez");
        Contrato contrato1 = new Contrato(quintero1, "2-4-2000", "1-1-2001", 20000, 58, "Juan Carlos", 21454);
        Contrato contrato2 = new Contrato(quintero1, "4-6-1997", "12-5-2005", 8900, 65, "Mario", 435);
        listaContratos.agregarContrato(contrato1);
        listaContratos.agregarContrato(contrato2);
        listaContratos.modificarContrato(65, new Contrato(quintero1, "4-6-1997", "12-5-2005", 8900, 45, "Mario", 435));
        assertEquals(Integer.valueOf(8900), listaContratos.getContrato(45).getMontoMensual());
    }

    @Test
    public void eliminarContrato() throws ContratoExistenteException, ContratoInexistenteException{
        RepoContratos listaContratos = new RepoContratos();
        Cliente quintero1 = new Quintero("27-99999991-1", "Pedro Sanchez 13", "quintero@gmail.com", "3834951245", "Gustavo", "Martinez");
        Contrato contrato1 = new Contrato(quintero1, "2-4-2000", "1-1-2001", 20000, 58, "Juan Carlos", 21454);
        Contrato contrato2 = new Contrato(quintero1, "4-6-1997", "12-5-2005", 8900, 65, "Mario", 435);
        listaContratos.agregarContrato(contrato1);
        listaContratos.agregarContrato(contrato2);
        listaContratos.eliminarContrato(65);
        assertEquals(1, listaContratos.getListaContratos().size());
    }

    @Test (expected = ContratoExistenteException.class)
    public void AgregarContratoException() throws ContratoExistenteException{
        RepoContratos listaContratos = new RepoContratos();
        Cliente quintero1 = new Quintero("27-99999991-1", "Pedro Sanchez 13", "quintero@gmail.com", "3834951245", "Gustavo", "Martinez");
        Contrato contrato1 = new Contrato(quintero1, "2-4-2000", "1-1-2001", 20000, 58, "Juan Carlos", 21454);
        listaContratos.agregarContrato(contrato1);
        listaContratos.agregarContrato(contrato1);
    }

    @Test (expected = ContratoInexistenteException.class)
    public void leerContratoException() throws ContratoExistenteException, ContratoInexistenteException{
        RepoContratos listaContratos = new RepoContratos();
        listaContratos.getContrato(58);
    }
    
}
