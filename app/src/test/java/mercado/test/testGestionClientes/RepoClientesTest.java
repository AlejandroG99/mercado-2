package mercado.test.testGestionClientes;

import static org.junit.Assert.assertEquals;

import org.junit.Test;

import mercado.gestionClientes.ClienteExistenteException;
import mercado.gestionClientes.ClienteInexistenteException;
import mercado.gestionClientes.Empresa;
import mercado.gestionClientes.Quintero;
import mercado.gestionClientes.RepoClientes;

public class RepoClientesTest {
    
    @Test
    public void AgregarCliente() throws ClienteExistenteException {
        RepoClientes listaClientes = new RepoClientes();
        Empresa empresa1 = new Empresa("27-00000001-1", "Zurita 2154", "empresaej@gmail.com", "3834123456", "Aspen SRL", "RASOCIAL");
        Quintero quintero1 = new Quintero("27-99999991-1", "Pedro Sanchez 13", "quintero@gmail.com", "3834951245", "Gustavo", "Martinez");
        listaClientes.agregarCliente(empresa1);
        listaClientes.agregarCliente(quintero1);
        assertEquals(2, listaClientes.getListaClientes().size());
    }

    @Test
    public void leerCliente() throws ClienteExistenteException, ClienteInexistenteException {
        RepoClientes listaClientes = new RepoClientes();
        Empresa empresa1 = new Empresa("27-00000001-1", "Zurita 2154", "empresaej@gmail.com", "3834123456", "Aspen SRL", "RASOCIAL");
        Quintero quintero1 = new Quintero("27-99999991-1", "Pedro Sanchez 13", "quintero@gmail.com", "3834951245", "Gustavo", "Martinez");
        listaClientes.agregarCliente(quintero1);
        listaClientes.agregarCliente(empresa1);
        assertEquals(quintero1, listaClientes.getCliente("27-99999991-1"));
    }

    @Test
    public void modificarCliente() throws ClienteExistenteException, ClienteInexistenteException {
        RepoClientes listaClientes = new RepoClientes();
        Empresa empresa1 = new Empresa("27-00000001-1", "Zurita 2154", "empresaej@gmail.com", "3834123456", "Aspen SRL", "RASOCIAL");
        Quintero quintero1 = new Quintero("27-99999991-1", "Pedro Sanchez 13", "quintero@gmail.com", "3834951245", "Gustavo", "Martinez");
        listaClientes.agregarCliente(quintero1);
        listaClientes.agregarCliente(empresa1);
        listaClientes.modificarCliente("27-00000001-1", new Empresa("27-00000001-1", "Zurita 2154", "empresaej@gmail.com", "1122334455", "Aspen SRL", "RASOCIAL"));
        assertEquals("1122334455", listaClientes.getCliente("27-00000001-1").getTelefono());
    }

    @Test
    public void eliminarClienteCliente() throws ClienteExistenteException, ClienteInexistenteException {
        RepoClientes listaClientes = new RepoClientes();
        Empresa empresa1 = new Empresa("27-00000001-1", "Zurita 2154", "empresaej@gmail.com", "3834123456", "Aspen SRL", "RASOCIAL");
        Quintero quintero1 = new Quintero("27-99999991-1", "Pedro Sanchez 13", "quintero@gmail.com", "3834951245", "Gustavo", "Martinez");
        listaClientes.agregarCliente(quintero1);
        listaClientes.agregarCliente(empresa1);
        listaClientes.eliminarCliente("27-00000001-1");
        assertEquals(1, listaClientes.getListaClientes().size());
    }

    @Test (expected = ClienteExistenteException.class)
    public void AgregarClienteException() throws ClienteExistenteException {
        RepoClientes listaClientes = new RepoClientes();
        Empresa empresa1 = new Empresa("27-00000001-1", "Zurita 2154", "empresaej@gmail.com", "3834123456", "Aspen SRL", "RASOCIAL");
        listaClientes.agregarCliente(empresa1);
        listaClientes.agregarCliente(empresa1);
    }

    @Test (expected = ClienteInexistenteException.class)
    public void leerClienteException() throws ClienteExistenteException, ClienteInexistenteException {
        RepoClientes listaClientes = new RepoClientes();
        listaClientes.getCliente("27-99999991-1");
    }
}
