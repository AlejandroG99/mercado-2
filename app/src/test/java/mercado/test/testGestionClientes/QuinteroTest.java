package mercado.test.testGestionClientes; 

import org.junit.Test;

import mercado.gestionClientes.Quintero;

import static org.junit.Assert.*;

public class QuinteroTest {
    
    @Test
    public void testObternerIdentificacionFiscalQuintero(){
        Quintero quintero1 = new Quintero("27-99999991-1", "Pedro Sanchez 13", "quintero@gmail.com", "3834951245", "Gustavo", "Martinez");
        assertEquals("27-99999991-1", quintero1.getIdentificacionFiscal());  
    }

    @Test
    public void testDireccionQuintero(){
        Quintero quintero1 = new Quintero("27-99999991-1", "Pedro Sanchez 13", "quintero@gmail.com", "3834951245", "Gustavo", "Martinez");
        assertEquals("Pedro Sanchez 13", quintero1.getDireccion());  
    }

    @Test
    public void testEmailQuintero(){
        Quintero quintero1 = new Quintero("27-99999991-1", "Pedro Sanchez 13", "quintero@gmail.com", "3834951245", "Gustavo", "Martinez");
        assertEquals("quintero@gmail.com", quintero1.getEmail());
    }

    @Test
    public void testTelefonoQuintero(){
        Quintero quintero1 = new Quintero("27-99999991-1", "Pedro Sanchez 13", "quintero@gmail.com", "3834951245", "Gustavo", "Martinez");
        assertEquals("3834951245", quintero1.getTelefono());  
    }

    @Test
    public void testObternerNombreQuintero(){
        Quintero quintero1 = new Quintero("27-99999991-1", "Pedro Sanchez 13", "quintero@gmail.com", "3834951245", "Gustavo", "Martinez");
        assertEquals("Gustavo", quintero1.getNombre());  
    }

    @Test
    public void testObternerRazonSocialQuintero(){
        Quintero quintero1 = new Quintero("27-99999991-1", "Pedro Sanchez 13", "quintero@gmail.com", "3834951245", "Gustavo", "Martinez");
        assertEquals("Martinez", quintero1.getApellido());  
    }

    @Test
    public void testQuinterosIguales(){
        Quintero quintero1 = new Quintero("27-99999991-1", "Pedro Sanchez 13", "quintero@gmail.com", "3834951245", "Gustavo", "Martinez");
        Quintero quintero2 = new Quintero("27-99999991-1", "Pedro Sanchez 13", "quintero@gmail.com", "3834951245", "Gustavo", "Martinez");

        assertEquals(true, quintero1.equals(quintero2));
    }

    @Test
    public void testQuinterosDistintos(){
        Quintero quintero1 = new Quintero("27-99999991-1", "Pedro Sanchez 13", "quintero@gmail.com", "3834951245", "Gustavo", "Martinez");
        Quintero quintero2 = new Quintero("27-99999999-1", "Alem 54", "quintero2@gmail.com", "3834447585", "Mario", "Gutierres");

        assertEquals(false, quintero1.equals(quintero2));
    }
    
}