package mercado.test.testGestionClientes; 

import org.junit.Test;

import mercado.gestionClientes.Empresa;

import static org.junit.Assert.*;

public class EmpresaTest {
    
    @Test
    public void testObternerIdentificacionFiscalEmpresa(){
        Empresa empresa1 = new Empresa("27-00000001-1", "Zurita 2154", "empresaej@gmail.com", "3834123456", "Aspen SRL", "RASOCIAL");
        assertEquals("27-00000001-1", empresa1.getIdentificacionFiscal());  
    }

    @Test
    public void testDireccionEmpresa(){
        Empresa empresa1 = new Empresa("27-00000001-1", "Zurita 2154", "empresaej@gmail.com", "3834123456", "Aspen SRL", "RASOCIAL");
        assertEquals("Zurita 2154", empresa1.getDireccion());  
    }

    @Test
    public void testEmailEmpresa(){
        Empresa empresa1 = new Empresa("27-00000001-1", "Zurita 2154", "empresaej@gmail.com", "3834123456", "Aspen SRL", "RASOCIAL");
        assertEquals("empresaej@gmail.com", empresa1.getEmail());  
    }

    @Test
    public void testTelefonoEmpresa(){
        Empresa empresa1 = new Empresa("27-00000001-1", "Zurita 2154", "empresaej@gmail.com", "3834123456", "Aspen SRL", "RASOCIAL");
        assertEquals("3834123456", empresa1.getTelefono());  
    }

    @Test
    public void testObternerNombreEmpresa(){
        Empresa empresa1 = new Empresa("27-00000001-1", "Zurita 2154", "empresaej@gmail.com", "3834123456", "Aspen SRL", "RASOCIAL");
        assertEquals("Aspen SRL", empresa1.getNombreEmpresa());  
    }

    @Test
    public void testObternerRazonSocialEmpresa(){
        Empresa empresa1 = new Empresa("27-00000001-1", "Zurita 2154", "empresaej@gmail.com", "3834123456", "Aspen SRL", "RASOCIAL");
        assertEquals("RASOCIAL", empresa1.getRazonSocial());  
    }
    
}