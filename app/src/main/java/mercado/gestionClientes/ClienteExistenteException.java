package mercado.gestionClientes;

public class ClienteExistenteException extends Exception {
    public ClienteExistenteException(){
        super("Este cliente ya existe en la base de datos de clientes.");
    }
}
