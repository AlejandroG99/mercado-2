package mercado.gestionClientes;

import java.util.ArrayList;

public class RepoClientes {
    private ArrayList<Cliente> listaClientes;

    public RepoClientes() {
        setListaClientes(new ArrayList<Cliente>());
    }

    public void setListaClientes(ArrayList<Cliente> listaClientes) {
        this.listaClientes = listaClientes;
    }

    public ArrayList<Cliente> getListaClientes() {
        return listaClientes;
    }

    public void agregarCliente(Cliente cliente) throws ClienteExistenteException{
        
        for (Cliente var : listaClientes) {
            if (var.equals(cliente)) {
                throw new ClienteExistenteException();
            }
        }
        
        listaClientes.add(cliente);
    }

    public Cliente getCliente(String identificacionFiscal) throws ClienteInexistenteException{
        
        Cliente ClienteEncontrado = null;
        
        for (Cliente var : listaClientes) {
            if (var.getIdentificacionFiscal().equals(identificacionFiscal)) {
                ClienteEncontrado = var;
                break;
            }
        }
        
        if (ClienteEncontrado == null) {
            throw new ClienteInexistenteException();
        }
        
        return ClienteEncontrado;
    }

    public void modificarCliente(String identificacionFiscal, Cliente ClienteAct) throws ClienteInexistenteException{
        Cliente ClienteEncontrado = null;
        ClienteEncontrado = getCliente(identificacionFiscal);
        listaClientes.remove(ClienteEncontrado); 
        listaClientes.add(ClienteAct);           
	} 

    public void eliminarCliente(String identificacionFiscal) throws ClienteInexistenteException{
        
        Cliente ClienteEncontrado = null;
        
        for (Cliente var : listaClientes) {
            if (var.getIdentificacionFiscal().equals(identificacionFiscal)) {
                ClienteEncontrado = var;
                break;
            }
        }
        
        if (ClienteEncontrado == null) {
            throw new ClienteInexistenteException();
        }

        listaClientes.remove(ClienteEncontrado);
    }
}