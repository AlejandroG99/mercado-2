package mercado.gestionClientes;

public abstract class Cliente {
    private String identificacionFiscal;
    private String direccion;
    private String email;
    private String telefono;

    public Cliente(String identificacionFiscal, String direccion, String email, String telefono) {
        this.identificacionFiscal = identificacionFiscal;
        this.direccion = direccion;
        this.email = email;
        this.telefono = telefono;
    }

    public String getIdentificacionFiscal() {
        return identificacionFiscal;
    }

    public String getDireccion() {
        return direccion;
    }

    public String getEmail() {
        return email;
    }

    public String getTelefono() {
        return telefono;
    }

    public void setIdentificacionFiscal(String identificacionFiscal) {
        this.identificacionFiscal = identificacionFiscal;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public void settelefono(String telefono) {
        this.telefono = telefono;
    }

    @Override
    public abstract boolean equals(Object obj);
}
