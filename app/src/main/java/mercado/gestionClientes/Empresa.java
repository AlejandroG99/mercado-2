package mercado.gestionClientes;

public class Empresa extends Cliente{
    private String nombreEmpresa;
    private String razonSocial;
    
    public Empresa(String identificacionFiscal, String direccion, String email, String telefono, String nombreEmpresa, String razonSocial) {
        super(identificacionFiscal,  direccion,  email,  telefono);
        this.nombreEmpresa = nombreEmpresa;
        this.razonSocial = razonSocial;
    }
    public String getNombreEmpresa() {
        return nombreEmpresa;
    }
    public void setNombreEmpresa(String nombreEmpresa) {
        this.nombreEmpresa = nombreEmpresa;
    }
    public String getRazonSocial() {
        return razonSocial;
    }
    public void setRazonSocial(String razonSocial) {
        this.razonSocial = razonSocial;
    }
    
    public boolean equals(Object obj){
        Cliente aComparar=(Cliente)obj;
        return this.getIdentificacionFiscal().equals(aComparar.getIdentificacionFiscal());
    }
    
}
