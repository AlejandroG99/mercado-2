package mercado.gestionClientes;

public class ClienteInexistenteException extends Exception {
    public ClienteInexistenteException(){
        super("Este cliente no existe en la base de datos de clientes.");
    }
}
