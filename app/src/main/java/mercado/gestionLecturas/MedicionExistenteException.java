package mercado.gestionLecturas;

public class MedicionExistenteException extends Exception {
    public MedicionExistenteException(){
        super("Esta medición ya existe en la base de datos del medidor.");
    }
}
