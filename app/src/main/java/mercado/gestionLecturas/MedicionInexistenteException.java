package mercado.gestionLecturas;

public class MedicionInexistenteException extends Exception {
    public MedicionInexistenteException(){
        super("Esta medición no existe en la base de datos del medidor.");
    }
}
