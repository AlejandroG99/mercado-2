package mercado.gestionLecturas;

import java.util.ArrayList;

public class MedidorLuz {
    private ArrayList<Lectura> listaLecturas;
    
    public MedidorLuz() {
        setListaLecturas(new ArrayList<Lectura>());
    }

    public void setListaLecturas(ArrayList<Lectura> listaLecturas) {
        this.listaLecturas = listaLecturas;
    }

    public ArrayList<Lectura> getListaLecturas() {
        return listaLecturas;
    }

    public void agregarLectura(Lectura lectura) throws MedicionExistenteException{
        
        for (Lectura var : listaLecturas) {
            if (var.equals(lectura)) {
                throw new MedicionExistenteException();
            }
        }
        
        this.listaLecturas.add(lectura);
    }

    public Lectura getLectura(Integer numeroLectura) throws MedicionInexistenteException{
        
        Lectura lecturaEncontrada = null;
        
        for (Lectura var : listaLecturas) {
            if (var.getLectura().equals(numeroLectura)) {
                lecturaEncontrada = var;
                break;
            }
        }
        
        if (lecturaEncontrada == null) {
            throw new MedicionInexistenteException();
        }
        
        return lecturaEncontrada;
    }

    public void modificarLectura(Integer numeroLectura, Lectura lecturaAct) throws MedicionInexistenteException{
        Lectura lecturaEncontrada = null;
        lecturaEncontrada = getLectura(numeroLectura);
        listaLecturas.remove(lecturaEncontrada); 
        listaLecturas.add(lecturaAct);           
	} 

    public void eliminarLectura(Integer numeroLectura) throws MedicionInexistenteException{
        
        Lectura lecturaEncontrada = null;
        
        for (Lectura var : listaLecturas) {
            if (var.getLectura().equals(numeroLectura)) {
                lecturaEncontrada = var;
                break;
            }
        }
        
        if (lecturaEncontrada == null) {
            throw new MedicionInexistenteException();
        }

        listaLecturas.remove(lecturaEncontrada);
    }
}