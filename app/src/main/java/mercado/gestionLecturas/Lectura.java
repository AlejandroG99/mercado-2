package mercado.gestionLecturas;

public class Lectura {
    private Integer lectura;

    public Lectura(Integer lectura) {
        this.lectura = lectura;
    }

    public Integer getLectura() {
        return lectura;
    }

    public void setLectura(Integer lectura) {
        this.lectura = lectura;
    }

    @Override
    public boolean equals(Object obj) {
        Lectura other = (Lectura) obj;
        return lectura.equals(other.getLectura());
    }

}
