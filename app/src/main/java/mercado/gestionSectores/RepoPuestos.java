package mercado.gestionSectores;

import java.util.ArrayList;

public class RepoPuestos {
    private ArrayList<Puesto> listaPuestos;
    
    public RepoPuestos() {
        setListaPuestos(new ArrayList<Puesto>());
    }

    public void setListaPuestos(ArrayList<Puesto> listaPuestos) {
        this.listaPuestos = listaPuestos;
    }

    public ArrayList<Puesto> getListaPuestos() {
        return listaPuestos;
    }
    
    public void agregarPuesto(Puesto puesto) throws PuestoExistenteException{
        
        for (Puesto var : listaPuestos) {
            if (var.equals(puesto)) {
                throw new PuestoExistenteException();
            }
        }
        
        this.listaPuestos.add(puesto);
    }

    public Puesto getPuesto(Integer numeroPuesto) throws PuestoInexistenteException{
        
        Puesto puestoEncontrado = null;
        
        for (Puesto var : listaPuestos) {
            if (var.getNumeroPuesto().equals(numeroPuesto)) {
                puestoEncontrado = var;
                break;
            }
        }
        
        if (puestoEncontrado == null) {
            throw new PuestoInexistenteException();
        }
        
        return puestoEncontrado;
    }

    public void modificarPuesto(Integer numeroPuesto, Puesto puestoAct) throws PuestoInexistenteException{
        Puesto puestoEncontrado = null;
        puestoEncontrado = getPuesto(numeroPuesto);
        listaPuestos.remove(puestoEncontrado); 
        listaPuestos.add(puestoAct);           
	} 

    public void eliminarPuesto(Integer numeroPuesto) throws PuestoInexistenteException{
        
        Puesto puestoEncontrado = null;
        
        for (Puesto var : listaPuestos) {
            if (var.getNumeroPuesto().equals(numeroPuesto)) {
                puestoEncontrado = var;
                break;
            }
        }
        
        if (puestoEncontrado == null) {
            throw new PuestoInexistenteException();
        }

        listaPuestos.remove(puestoEncontrado);
    }
    
    public Integer size(){
        return listaPuestos.size();
    }
    
    public boolean isEmpty(){
        return listaPuestos.isEmpty();
    }
}