package mercado.gestionSectores;

import mercado.gestionLecturas.MedidorLuz;

public class Puesto {
    private Integer numeroPuesto;
    private String tipoPuesto;
    private Integer ancho;
    private Integer largo;
    private Boolean conTecho;
    private Boolean conCamaraRefrigerante;
    private Boolean disponibilidad;
    private MedidorLuz medidor;
    
    public Puesto(Integer numeroPuesto, String tipoPuesto, Integer ancho, Integer largo,
            Boolean conTecho, Boolean conCamaraRefrigerante, Boolean disponibilidad,
            MedidorLuz medidor) {
        this.numeroPuesto = numeroPuesto;
        this.tipoPuesto = tipoPuesto;
        this.ancho = ancho;
        this.largo = largo;
        this.conTecho = conTecho;
        this.conCamaraRefrigerante = conCamaraRefrigerante;
        this.disponibilidad = disponibilidad;
        this.medidor = medidor;
    }

    public Integer getNumeroPuesto() {
        return numeroPuesto;
    }

    public void setNumeroPuestor(Integer numeroPuesto) {
        this.numeroPuesto = numeroPuesto;
    }

    public String getTipoPuesto() {
        return tipoPuesto;
    }

    public void setTipoPuesto(String tipoPuesto) {
        this.tipoPuesto = tipoPuesto;
    }

    public Integer getAncho() {
        return ancho;
    }

    public void setAncho(Integer ancho) {
        this.ancho = ancho;
    }

    public Integer getLargo() {
        return largo;
    }

    public void setLargo(Integer largo) {
        this.largo = largo;
    }

    public Boolean getConTecho() {
        return conTecho;
    }

    public void setConTecho(Boolean conTecho) {
        this.conTecho = conTecho;
    }

    public Boolean getConCamaraRefrigerante() {
        return conCamaraRefrigerante;
    }

    public void setConCamaraRefrigerante(Boolean conCamaraRefrigerante) {
        this.conCamaraRefrigerante = conCamaraRefrigerante;
    }

    public Boolean getDisponibilidad() {
        return disponibilidad;
    }

    public void setDisponibilidad(Boolean disponibilidad) {
        this.disponibilidad = disponibilidad;
    }

    public MedidorLuz getMedidor() {
        return medidor;
    }

    public void setMedidor(MedidorLuz medidor) {
        this.medidor = medidor;
    }

    @Override
    public boolean equals(Object obj) {
        Puesto puesto = (Puesto) obj;
        return numeroPuesto.equals(puesto.getNumeroPuesto());
    }
}
