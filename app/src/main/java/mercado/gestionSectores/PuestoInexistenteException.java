package mercado.gestionSectores;

public class PuestoInexistenteException extends Exception {
    public PuestoInexistenteException () {
        super("Este puesto no existe en la base de datos del sector.");
    }
}
