package mercado.gestionSectores;

import java.util.ArrayList;

public class RepoSectores{
    
    private ArrayList<Sector> listaSectores;

    public RepoSectores() {
        setlistaSectores(new ArrayList<Sector>());
    }

    public void setlistaSectores(ArrayList<Sector> listaSectores) {
        this.listaSectores = listaSectores;
    }

    public ArrayList<Sector> getlistaSectores() {
        return listaSectores;
    }
    
    public void agregarSector(Sector sector) throws SectorExistenteException{
        
        for (Sector var : listaSectores) {
            if (var.equals(sector)) {
                throw new SectorExistenteException();
            }
        }
        
        this.listaSectores.add(sector);
    }

    public Sector getSector(String nombreSector) throws SectorInexistenteException{
        
        Sector sectorEncontrado = null;
        
        for (Sector var : listaSectores) {
            if (var.getNombreSector().equals(nombreSector)) {
                sectorEncontrado = var;
                break;
            }
        }
        
        if (sectorEncontrado == null) {
            throw new SectorInexistenteException();
        }
        
        return sectorEncontrado;
    }

    public void modificarSector(String nombreSector, Sector sectorAct) throws SectorInexistenteException{ 
        Sector sectorEncontrado = null;
        sectorEncontrado = getSector(nombreSector);
        listaSectores.remove(sectorEncontrado); 
        listaSectores.add(sectorAct);
    }

    public void eliminarSector(String nombreSector) throws SectorInexistenteException{
        
        Sector sectorEncontrado = null;
        
        for (Sector var : listaSectores) {
            if (var.getNombreSector().equals(nombreSector)) {
                sectorEncontrado = var;
                break;
            }
        }
        
        if (sectorEncontrado == null) {
            throw new SectorInexistenteException();
        }

        listaSectores.remove(sectorEncontrado);
    }
    
    public Integer size(){
        return listaSectores.size();
    }
    
    public Boolean isEmpty(){
        return listaSectores.isEmpty();
    }
}
