package mercado.gestionSectores;

public class SectorExistenteException extends Exception {
    public SectorExistenteException () {
        super("Este sector ya existe en la base de datos del mercado.");
    }    
}
