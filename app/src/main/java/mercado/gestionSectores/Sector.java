package mercado.gestionSectores;

public class Sector {
    private Integer numeroPuestos;
    private String nombreSector;
    private RepoPuestos listaPuestos;
    
    public Sector(Integer numeroPuestos, String nombreSector) {
        this.numeroPuestos = numeroPuestos;
        this.nombreSector = nombreSector;
        setListaPuestos(new RepoPuestos());
    }

    public void setListaPuestos(RepoPuestos planificacion) {
        this.listaPuestos = planificacion;
    }

    public Integer getNumeroPuestos() {
        return numeroPuestos;
    }

    public void setNumeroPuestos(Integer numeroPuestos) {
        this.numeroPuestos = numeroPuestos;
    }

    public String getNombreSector() {
        return nombreSector;
    }

    public void setNombreSector(String nombreSector) {
        this.nombreSector = nombreSector;
    }

    public RepoPuestos getRepoPuestos() {
        return listaPuestos;
    }

    public void setRepoPuestos(RepoPuestos puestos) {
        this.listaPuestos = puestos;
    }
    
    public void agregarPuesto(Puesto puesto) throws PuestoExistenteException {
        for (Puesto var : listaPuestos.getListaPuestos()) {
            if (var.getNumeroPuesto().equals(puesto.getNumeroPuesto())) {
                throw new PuestoExistenteException();
            }
        }
        this.listaPuestos.getListaPuestos().add(puesto);
    }

    @Override
    public boolean equals(Object obj) {
        Sector sector = (Sector) obj;
        return listaPuestos.equals(sector.getRepoPuestos());
    }
    
}

