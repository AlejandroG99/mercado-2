package mercado.gestionSectores;

public class PuestoExistenteException extends Exception{
    public PuestoExistenteException () {
        super("Este puesto ya existe en la base de datos del sector.");
    }
}
