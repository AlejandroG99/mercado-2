package mercado.gestionSectores;

public class SectorInexistenteException extends Exception{
    public SectorInexistenteException(){
        super("Este sector no existe en la base de datos del mercado.");
    }
}
