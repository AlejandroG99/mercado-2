package mercado.gestionContratos;

import java.util.ArrayList;

public class RepoContratos {
    private ArrayList<Contrato> listaContratos;

    public RepoContratos() {
        setListaContratos(new ArrayList<Contrato>());
    }

    public void setListaContratos(ArrayList<Contrato> listaContratos) {
        this.listaContratos = listaContratos;
    }

    public ArrayList<Contrato> getListaContratos() {
        return listaContratos;
    }

    public void agregarContrato(Contrato contrato) throws ContratoExistenteException{
        
        for (Contrato var : listaContratos) {
            if (var.equals(contrato)) {
                throw new ContratoExistenteException();
            }
        }
        
        this.listaContratos.add(contrato);
    }

    public Contrato getContrato(Integer numeroContrato) throws ContratoInexistenteException{
        
        Contrato contratoEncontrado = null;
        
        for (Contrato var : listaContratos) {
            if (var.getNumeroContrato().equals(numeroContrato)) {
                contratoEncontrado = var;
                break;
            }
        }
        
        if (contratoEncontrado == null) {
            throw new ContratoInexistenteException();
        }
        
        return contratoEncontrado;
    }

    public void modificarContrato(Integer numeroContrato, Contrato contratoAct) throws ContratoInexistenteException{
        Contrato contratoEncontrado = null;
        contratoEncontrado = getContrato(numeroContrato);
        listaContratos.remove(contratoEncontrado); 
        listaContratos.add(contratoAct);           
	} 

    public void eliminarContrato(Integer numeroContrato) throws ContratoInexistenteException{
        
        Contrato contratoEncontrado = null;
        
        for (Contrato var : listaContratos) {
            if (var.getNumeroContrato().equals(numeroContrato)) {
                contratoEncontrado = var;
                break;
            }
        }
        
        if (contratoEncontrado == null) {
            throw new ContratoInexistenteException();
        }

        listaContratos.remove(contratoEncontrado);
    }
}