package mercado.gestionContratos;

import mercado.gestionClientes.Cliente;

public class Contrato {
    private Integer numeroContrato;
    private Cliente clienteAlquilante;
    private String inicio;
    private String fin;
    private Integer montoMensual;
    private String responsableMercado;
    private final Integer ultimaMedicionLuz;
    
    public Contrato(Cliente clienteAlquilante, String inicio, String fin, Integer montoMensual, Integer idContrato, String responsableMercado,
            Integer ultimaMedicionLuz) {
        this.clienteAlquilante = clienteAlquilante;
        this.inicio = inicio;
        this.fin = fin;
        this.montoMensual = montoMensual;
        this.numeroContrato = idContrato;
        this.responsableMercado = responsableMercado;
        this.ultimaMedicionLuz = ultimaMedicionLuz;
    }

    public Cliente getClienteAlquilante() {
        return clienteAlquilante;
    }

    public void setClienteAlquilante(Cliente clienteAlquilante) {
        this.clienteAlquilante = clienteAlquilante;
    }

    public String getInicio() {
        return inicio;
    }

    public void setInicio(String inicio) {
        this.inicio = inicio;
    }

    public String getFin() {
        return fin;
    }

    public void setFin(String fin) {
        this.fin = fin;
    }

    public Integer getMontoMensual() {
        return montoMensual;
    }

    public void setMontoMensual(Integer montoMensual) {
        this.montoMensual = montoMensual;
    }

    public Integer getNumeroContrato() {
        return numeroContrato;
    }

    public void setNumeroContrato(Integer numeroContrato) {
        this.numeroContrato = numeroContrato;
    }

    public String getResponsableMercado() {
        return responsableMercado;
    }

    public void setResponsableMercado(String responsableMercado) {
        this.responsableMercado = responsableMercado;
    }

    public Integer getUltimaMedicionLuz() {
        return ultimaMedicionLuz;
    }

    @Override

    public boolean equals(Object o) {
        Contrato aComparar = (Contrato)o;
        return this.getNumeroContrato().equals(aComparar.getNumeroContrato());
    }
}
