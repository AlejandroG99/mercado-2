package mercado.gestionContratos;

public class ContratoInexistenteException extends Exception {
    public ContratoInexistenteException(){
        super("Este contrato no existe en la base de datos de contratos.");
    }
}
