package mercado.gestionContratos;

public class ContratoExistenteException extends Exception {
    public ContratoExistenteException(){
        super("Este contrato ya existe en la base de datos de contratos.");
    }
}
