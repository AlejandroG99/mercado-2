package mercado;

/*import igu.iguContrato.Contrato;
import igu.iguPuesto.Puesto;
import java.util.ArrayList;
import mercado.gestionClientes.Cliente;*/
import mercado.gestionClientes.RepoClientes;
import mercado.gestionContratos.RepoContratos;
import mercado.gestionSectores.RepoPuestos;
import mercado.gestionSectores.RepoSectores;
//import mercado.gestionSectores.Sector;


public class Mercado {
    /*
    private ArrayList<Sector> listaSectores = new ArrayList<Sector>();
    private ArrayList<Puesto> listaPuestos = new ArrayList<Puesto>();
    private ArrayList<Contrato> listaContratos = new ArrayList<Contrato>();
    private ArrayList<Cliente> listaClientes = new ArrayList<Cliente>();
    */
    private RepoSectores listaSectores = new RepoSectores();
    private RepoPuestos listaPuestos = new RepoPuestos();
    private RepoContratos listaContratos = new RepoContratos();
    private RepoClientes listaClientes = new RepoClientes();
    
    private static Mercado mercado;
    
    private Mercado() {
    
    }
    
    public static Mercado instancia() {
        if(mercado==null){
            mercado = new Mercado();
        }
        return mercado;
    }
    /*
    public ArrayList<Sector> getListaSectores() {
        return listaSectores;
    }

    public ArrayList<Puesto> getListaPuestos() {
        return listaPuestos;
    }

    public ArrayList<Contrato> getListaContratos() {
        return listaContratos;
    }

    public ArrayList<Cliente> getListaClientes() {
        return listaClientes;
    }

    public static Mercado getMercado() {
        return mercado;
    }

    public void setListaSectores(ArrayList<Sector> listaSectores) {
        this.listaSectores = listaSectores;
    }

    public void setListaPuestos(ArrayList<Puesto> listaPuestos) {
        this.listaPuestos = listaPuestos;
    }

    public void setListaContratos(ArrayList<Contrato> listaContratos) {
        this.listaContratos = listaContratos;
    }

    public void setListaClientes(ArrayList<Cliente> listaClientes) {
        this.listaClientes = listaClientes;
    }
    */

    public RepoSectores getListaSectores() {
        return listaSectores;
    }

    public RepoPuestos getListaPuestos() {
        return listaPuestos;
    }

    public RepoContratos getListaContratos() {
        return listaContratos;
    }

    public RepoClientes getListaClientes() {
        return listaClientes;
    }

    public static Mercado getMercado() {
        return mercado;
    }

    public void setListaSectores(RepoSectores listaSectores) {
        this.listaSectores = listaSectores;
    }

    public void setListaPuestos(RepoPuestos listaPuestos) {
        this.listaPuestos = listaPuestos;
    }

    public void setListaContratos(RepoContratos listaContratos) {
        this.listaContratos = listaContratos;
    }

    public void setListaClientes(RepoClientes listaClientes) {
        this.listaClientes = listaClientes;
    }

    public static void setMercado(Mercado mercado) {
        Mercado.mercado = mercado;
    }

    
}

