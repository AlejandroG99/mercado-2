package igu;

import igu.iguContrato.Contrato;
import igu.iguCliente.ClienteEmpresa;
import igu.iguCliente.ClienteQuintero;
import igu.iguPuesto.PuestoWin;
import igu.iguSector.SectorWin;
import mercado.Mercado;

public class Principal extends javax.swing.JFrame {
    
    private final Mercado mercado;
    
    public Principal() {
        this.mercado = Mercado.instancia();
        initComponents();
    }

    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jMenu4 = new javax.swing.JMenu();
        jMenu5 = new javax.swing.JMenu();
        jMenu6 = new javax.swing.JMenu();
        jPanel1 = new javax.swing.JPanel();
        jMenuBar1 = new javax.swing.JMenuBar();
        jMenuSector = new javax.swing.JMenu();
        jMenuItemSector = new javax.swing.JMenuItem();
        jMenuPuesto = new javax.swing.JMenu();
        jMenuItemPuesto = new javax.swing.JMenuItem();
        jMenuCliente = new javax.swing.JMenu();
        jMenuItemQuintero = new javax.swing.JMenuItem();
        jMenuItemEmpresa = new javax.swing.JMenuItem();
        jMenuContrato = new javax.swing.JMenu();
        jMenuItemContrato = new javax.swing.JMenuItem();

        jMenu4.setText("jMenu4");

        jMenu5.setText("jMenu5");

        jMenu6.setText("jMenu6");

        setDefaultCloseOperation(javax.swing.WindowConstants.EXIT_ON_CLOSE);

        jPanel1.setBackground(new java.awt.Color(236, 223, 54));
        jPanel1.setForeground(new java.awt.Color(236, 223, 54));

        javax.swing.GroupLayout jPanel1Layout = new javax.swing.GroupLayout(jPanel1);
        jPanel1.setLayout(jPanel1Layout);
        jPanel1Layout.setHorizontalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 812, Short.MAX_VALUE)
        );
        jPanel1Layout.setVerticalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 488, Short.MAX_VALUE)
        );

        jMenuSector.setText("Sector");
        jMenuSector.setFont(new java.awt.Font("Dialog", 0, 24)); // NOI18N
        jMenuSector.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                jMenuSectorMouseClicked(evt);
            }
        });
        jMenuSector.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jMenuSectorActionPerformed(evt);
            }
        });

        jMenuItemSector.setFont(new java.awt.Font("Dialog", 0, 18)); // NOI18N
        jMenuItemSector.setText("Abrir");
        jMenuItemSector.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jMenuItemSectorActionPerformed(evt);
            }
        });
        jMenuSector.add(jMenuItemSector);

        jMenuBar1.add(jMenuSector);

        jMenuPuesto.setText("Puesto");
        jMenuPuesto.setFont(new java.awt.Font("Dialog", 0, 24)); // NOI18N

        jMenuItemPuesto.setFont(new java.awt.Font("Dialog", 0, 18)); // NOI18N
        jMenuItemPuesto.setText("Abrir");
        jMenuItemPuesto.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jMenuItemPuestoActionPerformed(evt);
            }
        });
        jMenuPuesto.add(jMenuItemPuesto);

        jMenuBar1.add(jMenuPuesto);

        jMenuCliente.setText("Cliente");
        jMenuCliente.setFont(new java.awt.Font("Dialog", 0, 24)); // NOI18N

        jMenuItemQuintero.setFont(new java.awt.Font("Dialog", 0, 18)); // NOI18N
        jMenuItemQuintero.setText("Quintero");
        jMenuItemQuintero.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jMenuItemQuinteroActionPerformed(evt);
            }
        });
        jMenuCliente.add(jMenuItemQuintero);

        jMenuItemEmpresa.setFont(new java.awt.Font("Dialog", 0, 18)); // NOI18N
        jMenuItemEmpresa.setText("Empresa");
        jMenuItemEmpresa.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jMenuItemEmpresaActionPerformed(evt);
            }
        });
        jMenuCliente.add(jMenuItemEmpresa);

        jMenuBar1.add(jMenuCliente);

        jMenuContrato.setText("Contrato");
        jMenuContrato.setFont(new java.awt.Font("Dialog", 0, 24)); // NOI18N
        jMenuContrato.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jMenuContratoActionPerformed(evt);
            }
        });

        jMenuItemContrato.setFont(new java.awt.Font("Dialog", 0, 18)); // NOI18N
        jMenuItemContrato.setText("Abrir");
        jMenuItemContrato.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jMenuItemContratoActionPerformed(evt);
            }
        });
        jMenuContrato.add(jMenuItemContrato);

        jMenuBar1.add(jMenuContrato);

        setJMenuBar(jMenuBar1);

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addComponent(jPanel1, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(0, 0, Short.MAX_VALUE))
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addComponent(jPanel1, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(0, 0, Short.MAX_VALUE))
        );

        pack();
    }// </editor-fold>//GEN-END:initComponents

    private void jMenuItemQuinteroActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jMenuItemQuinteroActionPerformed
        ClienteQuintero alta = new ClienteQuintero();
        alta.setVisible(true);
        alta.setLocationRelativeTo(null);
    }//GEN-LAST:event_jMenuItemQuinteroActionPerformed

    private void jMenuSectorMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_jMenuSectorMouseClicked
        
    }//GEN-LAST:event_jMenuSectorMouseClicked

    private void jMenuSectorActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jMenuSectorActionPerformed
         
    }//GEN-LAST:event_jMenuSectorActionPerformed

    private void jMenuContratoActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jMenuContratoActionPerformed
        
    }//GEN-LAST:event_jMenuContratoActionPerformed

    private void jMenuItemContratoActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jMenuItemContratoActionPerformed
        Contrato alta = new Contrato();
        alta.setVisible(true);
        alta.setLocationRelativeTo(null);  
    }//GEN-LAST:event_jMenuItemContratoActionPerformed

    private void jMenuItemPuestoActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jMenuItemPuestoActionPerformed
        dispose();
        PuestoWin alta = new PuestoWin(mercado.getListaSectores());
        alta.setVisible(true);
        alta.setLocationRelativeTo(null);   
    }//GEN-LAST:event_jMenuItemPuestoActionPerformed

    private void jMenuItemSectorActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jMenuItemSectorActionPerformed
        dispose();
        SectorWin alta = new SectorWin(mercado.getListaSectores());
        alta.setVisible(true);
        alta.setLocationRelativeTo(null);
    }//GEN-LAST:event_jMenuItemSectorActionPerformed

    private void jMenuItemEmpresaActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jMenuItemEmpresaActionPerformed
        ClienteEmpresa alta = new ClienteEmpresa();
        alta.setVisible(true);
        alta.setLocationRelativeTo(null);
    }//GEN-LAST:event_jMenuItemEmpresaActionPerformed

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JMenu jMenu4;
    private javax.swing.JMenu jMenu5;
    private javax.swing.JMenu jMenu6;
    private javax.swing.JMenuBar jMenuBar1;
    private javax.swing.JMenu jMenuCliente;
    private javax.swing.JMenu jMenuContrato;
    private javax.swing.JMenuItem jMenuItemContrato;
    private javax.swing.JMenuItem jMenuItemEmpresa;
    private javax.swing.JMenuItem jMenuItemPuesto;
    private javax.swing.JMenuItem jMenuItemQuintero;
    private javax.swing.JMenuItem jMenuItemSector;
    private javax.swing.JMenu jMenuPuesto;
    private javax.swing.JMenu jMenuSector;
    private javax.swing.JPanel jPanel1;
    // End of variables declaration//GEN-END:variables
}