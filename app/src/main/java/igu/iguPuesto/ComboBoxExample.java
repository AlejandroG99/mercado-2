package igu.iguPuesto;

import javax.swing.*;
import java.util.ArrayList;

public class ComboBoxExample {
    public static void main(String[] args) {
        // Crear y poblar el ArrayList
        ArrayList<String> options = new ArrayList<>();
        options.add("Opci�n 1");
        options.add("Opci�n 2");
        options.add("Opci�n 3");
        options.add("Opci�n 4");

        // Crear el JComboBox
        JComboBox<String> comboBox = new JComboBox<>();

        // Agregar los elementos del ArrayList al JComboBox
        for (String option : options) {
            comboBox.addItem(option);
        }

        // Configurar el JFrame para mostrar el JComboBox
        JFrame frame = new JFrame("Ejemplo de JComboBox");
        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        frame.setSize(300, 200);
        frame.setLayout(null);

        // Ubicar el JComboBox en el JFrame
        comboBox.setBounds(50, 50, 200, 30);
        frame.add(comboBox);

        // Hacer visible el JFrame
        frame.setVisible(true);
    }
}
