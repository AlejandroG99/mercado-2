package igu.iguPuesto;

import igu.Principal;
import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.JDialog;
import javax.swing.JFrame;
import javax.swing.JOptionPane;
import javax.swing.table.DefaultTableModel;
import mercado.gestionSectores.Puesto;
import mercado.gestionSectores.RepoPuestos;
import mercado.gestionSectores.RepoSectores;
import mercado.gestionSectores.Sector;
import mercado.gestionSectores.SectorInexistenteException;

public class PuestoWin extends javax.swing.JFrame {
    
    private RepoPuestos listaPuestos;
    private final RepoSectores listaSectores;
    private Integer numeroPuesto;
    private String nombreSector;
    
    public PuestoWin(RepoSectores listaSectores) {
        this.listaSectores = listaSectores;
        
        initComponents();
        
        String[] columnas = {"Sector", "Nro Puesto", "Tipo Puesto",
            "Ancho", "Largo", "Techo", "Cam. Refrig.", "Disponibilidad"};
        
        int totalPuestos = 0;
        
        for (Sector sector : listaSectores.getlistaSectores()) {
            totalPuestos += sector.getRepoPuestos().size();
        }
        
        Object[][] datos = new Object[totalPuestos][columnas.length];
        
        int index = 0;
        for (Sector sector : listaSectores.getlistaSectores()) {
            ArrayList<Puesto> puestosSector = sector.getRepoPuestos().getListaPuestos();
            for (Puesto puesto : puestosSector) {
                datos[index][0] = sector.getNombreSector();
                datos[index][1] = puesto.getNumeroPuesto();
                datos[index][2] = puesto.getTipoPuesto();
                datos[index][3] = puesto.getAncho();
                datos[index][4] = puesto.getLargo();
                datos[index][5] = puesto.getConTecho();
                datos[index][6] = puesto.getConCamaraRefrigerante();
                datos[index][7] = puesto.getDisponibilidad();
                index++;
            }
        }

        DefaultTableModel modelo = new DefaultTableModel(datos, columnas);
        tablaPuestos.setModel(modelo);

        setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
    }

    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jPanel1 = new javax.swing.JPanel();
        jLabel1 = new javax.swing.JLabel();
        jScrollPane1 = new javax.swing.JScrollPane();
        tablaPuestos = new javax.swing.JTable();
        jButtonMasInfo = new javax.swing.JButton();
        jButtonAgregar = new javax.swing.JButton();
        jButtonEliminar = new javax.swing.JButton();
        jButtonModificar = new javax.swing.JButton();
        jButtonSalir = new javax.swing.JButton();

        setDefaultCloseOperation(javax.swing.WindowConstants.EXIT_ON_CLOSE);

        jLabel1.setFont(new java.awt.Font("Dialog", 0, 48)); // NOI18N
        jLabel1.setText("Puesto");

        tablaPuestos.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {
                {null, null, null, null, null, null, null},
                {null, null, null, null, null, null, null},
                {null, null, null, null, null, null, null},
                {null, null, null, null, null, null, null}
            },
            new String [] {
                "N� Puesto", "Tipo Puesto", "Ancho", "Largo", "Techo", "Camara Refrigerante", "Disponibilidad"
            }
        ) {
            Class[] types = new Class [] {
                java.lang.String.class, java.lang.String.class, java.lang.Integer.class, java.lang.String.class, java.lang.Boolean.class, java.lang.Boolean.class, java.lang.Boolean.class
            };
            boolean[] canEdit = new boolean [] {
                false, false, false, false, false, false, false
            };

            public Class getColumnClass(int columnIndex) {
                return types [columnIndex];
            }

            public boolean isCellEditable(int rowIndex, int columnIndex) {
                return canEdit [columnIndex];
            }
        });
        jScrollPane1.setViewportView(tablaPuestos);
        if (tablaPuestos.getColumnModel().getColumnCount() > 0) {
            tablaPuestos.getColumnModel().getColumn(0).setResizable(false);
            tablaPuestos.getColumnModel().getColumn(1).setResizable(false);
            tablaPuestos.getColumnModel().getColumn(2).setResizable(false);
            tablaPuestos.getColumnModel().getColumn(3).setResizable(false);
            tablaPuestos.getColumnModel().getColumn(4).setResizable(false);
            tablaPuestos.getColumnModel().getColumn(5).setResizable(false);
            tablaPuestos.getColumnModel().getColumn(6).setResizable(false);
        }

        jButtonMasInfo.setFont(new java.awt.Font("Dialog", 0, 36)); // NOI18N
        jButtonMasInfo.setText("Mas Info");
        jButtonMasInfo.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButtonMasInfoActionPerformed(evt);
            }
        });

        jButtonAgregar.setFont(new java.awt.Font("Dialog", 0, 36)); // NOI18N
        jButtonAgregar.setText("Agregar");
        jButtonAgregar.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButtonAgregarActionPerformed(evt);
            }
        });

        jButtonEliminar.setFont(new java.awt.Font("Dialog", 0, 36)); // NOI18N
        jButtonEliminar.setText("Eliminar");
        jButtonEliminar.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButtonEliminarActionPerformed(evt);
            }
        });

        jButtonModificar.setFont(new java.awt.Font("Dialog", 0, 36)); // NOI18N
        jButtonModificar.setText("Modificar");
        jButtonModificar.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButtonModificarActionPerformed(evt);
            }
        });

        jButtonSalir.setFont(new java.awt.Font("Dialog", 0, 36)); // NOI18N
        jButtonSalir.setText("Salir");
        jButtonSalir.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButtonSalirActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout jPanel1Layout = new javax.swing.GroupLayout(jPanel1);
        jPanel1.setLayout(jPanel1Layout);
        jPanel1Layout.setHorizontalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel1Layout.createSequentialGroup()
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(jPanel1Layout.createSequentialGroup()
                        .addGap(33, 33, 33)
                        .addComponent(jScrollPane1, javax.swing.GroupLayout.PREFERRED_SIZE, 583, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(18, 18, 18)
                        .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(jButtonEliminar, javax.swing.GroupLayout.PREFERRED_SIZE, 184, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(jButtonAgregar, javax.swing.GroupLayout.PREFERRED_SIZE, 184, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(jButtonModificar, javax.swing.GroupLayout.PREFERRED_SIZE, 184, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(jButtonMasInfo, javax.swing.GroupLayout.PREFERRED_SIZE, 184, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(jButtonSalir, javax.swing.GroupLayout.PREFERRED_SIZE, 184, javax.swing.GroupLayout.PREFERRED_SIZE)))
                    .addGroup(jPanel1Layout.createSequentialGroup()
                        .addGap(338, 338, 338)
                        .addComponent(jLabel1)))
                .addContainerGap(32, Short.MAX_VALUE))
        );
        jPanel1Layout.setVerticalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel1Layout.createSequentialGroup()
                .addGap(14, 14, 14)
                .addComponent(jLabel1)
                .addGap(18, 18, 18)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                    .addGroup(jPanel1Layout.createSequentialGroup()
                        .addComponent(jButtonAgregar)
                        .addGap(30, 30, 30)
                        .addComponent(jButtonModificar)
                        .addGap(30, 30, 30)
                        .addComponent(jButtonEliminar)
                        .addGap(30, 30, 30)
                        .addComponent(jButtonMasInfo)
                        .addGap(30, 30, 30)
                        .addComponent(jButtonSalir))
                    .addComponent(jScrollPane1, javax.swing.GroupLayout.PREFERRED_SIZE, 0, Short.MAX_VALUE))
                .addContainerGap(29, Short.MAX_VALUE))
        );

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jPanel1, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jPanel1, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
        );

        pack();
    }// </editor-fold>//GEN-END:initComponents

    private void jButtonMasInfoActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButtonMasInfoActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_jButtonMasInfoActionPerformed

    private void jButtonAgregarActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButtonAgregarActionPerformed
        AgregarPuesto alta = new AgregarPuesto(listaSectores);
        alta.setVisible(true);
        alta.setLocationRelativeTo(null); 
        dispose();
    }//GEN-LAST:event_jButtonAgregarActionPerformed

    private void jButtonEliminarActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButtonEliminarActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_jButtonEliminarActionPerformed

    private void jButtonModificarActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButtonModificarActionPerformed
        if(tablaPuestos.getRowCount() > 0){
            if(tablaPuestos.getSelectedRow()!=-1){
                    nombreSector = tablaPuestos.getValueAt(tablaPuestos.getSelectedRow(), 0).toString(); 
                    numeroPuesto = Integer.parseInt(tablaPuestos.getValueAt(tablaPuestos.getSelectedRow(), 1).toString());

                    ModificarPuesto alta;
                try {
                    alta = new ModificarPuesto(nombreSector, numeroPuesto, listaSectores);
                    alta.setVisible(true);
                    alta.setLocationRelativeTo(null);
                } catch (SectorInexistenteException ex) {
                    Logger.getLogger(PuestoWin.class.getName()).log(Level.SEVERE, null, ex);
                }
                    

            }else{
                mostrarMensaje("No seleccion� un registro para eliminar", "Error", "Error al eliminar");
            }
        }else{
                mostrarMensaje("La tabla est� vac�a, no se puede eliminar", "Error", "Error al eliminar");
        }
        
        ModificarPuesto alta;
        try {
            alta = new ModificarPuesto(nombreSector, numeroPuesto, listaSectores);
            alta.setVisible(true);
            alta.setLocationRelativeTo(null);
        } catch (SectorInexistenteException ex) {
            Logger.getLogger(PuestoWin.class.getName()).log(Level.SEVERE, null, ex);
        }
         
    }//GEN-LAST:event_jButtonModificarActionPerformed

    private void jButtonSalirActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButtonSalirActionPerformed
        dispose();
        Principal alta = new Principal();
        alta.setVisible(true);
        alta.setLocationRelativeTo(null);
    }//GEN-LAST:event_jButtonSalirActionPerformed

    public void mostrarMensaje (String mensaje, String tipo, String titulo) { 
        JOptionPane optionPane = new JOptionPane (mensaje);
        if (tipo.equals("Info")) {
            optionPane.setMessageType (JOptionPane. INFORMATION_MESSAGE);
        }else if (tipo.equals("Error")) {
            optionPane.setMessageType(JOptionPane.ERROR_MESSAGE);
        }
        
        JDialog dialog = optionPane.createDialog(titulo);
        dialog.setAlwaysOnTop(true);
        dialog.setVisible(true);
    }
    
    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton jButtonAgregar;
    private javax.swing.JButton jButtonEliminar;
    private javax.swing.JButton jButtonMasInfo;
    private javax.swing.JButton jButtonModificar;
    private javax.swing.JButton jButtonSalir;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JPanel jPanel1;
    private javax.swing.JScrollPane jScrollPane1;
    private javax.swing.JTable tablaPuestos;
    // End of variables declaration//GEN-END:variables
}
