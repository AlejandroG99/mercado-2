package igu.iguSector;

import igu.Principal;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.JDialog;
import javax.swing.JFrame;
import javax.swing.JOptionPane;
import javax.swing.table.DefaultTableModel;
import mercado.gestionSectores.RepoSectores;
import mercado.gestionSectores.Sector;
import mercado.gestionSectores.SectorInexistenteException;

public class SectorWin extends javax.swing.JFrame {

    private final RepoSectores listaSectores;
    private String nombreSector;
    private Sector sectorElim;
    
    public SectorWin(RepoSectores listaSectores) {
        this.listaSectores = listaSectores;
        initComponents();
        
        String[] columnas = {"Nombre Sector", "N� Puestos"};
        Object[][] datos;

        datos = new Object[listaSectores.size()][2];
        int i = 0;
        for (Sector dato : listaSectores.getlistaSectores()) {
            datos[i][0] = dato.getNombreSector();
            datos[i][1] = dato.getNumeroPuestos();
            i++;
        }

        DefaultTableModel modelo = new DefaultTableModel(datos, columnas);
        this.tablaSectores.setModel(modelo);

        setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
    }

    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jButton1 = new javax.swing.JButton();
        jPanel1 = new javax.swing.JPanel();
        jLabel1 = new javax.swing.JLabel();
        jScrollPane1 = new javax.swing.JScrollPane();
        tablaSectores = new javax.swing.JTable();
        jButtonSalir = new javax.swing.JButton();
        jButtonAgregar = new javax.swing.JButton();
        jButtonEliminar = new javax.swing.JButton();
        jButtonModificar = new javax.swing.JButton();
        jButtonMasInfo = new javax.swing.JButton();

        jButton1.setText("jButton1");

        setDefaultCloseOperation(javax.swing.WindowConstants.EXIT_ON_CLOSE);

        jLabel1.setFont(new java.awt.Font("Dialog", 0, 48)); // NOI18N
        jLabel1.setText("Sector");

        tablaSectores.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {
                {null, null},
                {null, null},
                {null, null},
                {null, null}
            },
            new String [] {
                "Nombre Sector", "Nro de Puestos"
            }
        ) {
            Class[] types = new Class [] {
                java.lang.String.class, java.lang.Integer.class
            };
            boolean[] canEdit = new boolean [] {
                false, false
            };

            public Class getColumnClass(int columnIndex) {
                return types [columnIndex];
            }

            public boolean isCellEditable(int rowIndex, int columnIndex) {
                return canEdit [columnIndex];
            }
        });
        tablaSectores.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mousePressed(java.awt.event.MouseEvent evt) {
                tablaSectoresMousePressed(evt);
            }
        });
        jScrollPane1.setViewportView(tablaSectores);
        if (tablaSectores.getColumnModel().getColumnCount() > 0) {
            tablaSectores.getColumnModel().getColumn(0).setResizable(false);
            tablaSectores.getColumnModel().getColumn(1).setResizable(false);
        }

        jButtonSalir.setFont(new java.awt.Font("Dialog", 0, 36)); // NOI18N
        jButtonSalir.setText("Salir");
        jButtonSalir.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButtonSalirActionPerformed(evt);
            }
        });

        jButtonAgregar.setFont(new java.awt.Font("Dialog", 0, 36)); // NOI18N
        jButtonAgregar.setText("Agregar");
        jButtonAgregar.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButtonAgregarActionPerformed(evt);
            }
        });

        jButtonEliminar.setFont(new java.awt.Font("Dialog", 0, 36)); // NOI18N
        jButtonEliminar.setText("Eliminar");
        jButtonEliminar.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButtonEliminarActionPerformed(evt);
            }
        });

        jButtonModificar.setFont(new java.awt.Font("Dialog", 0, 36)); // NOI18N
        jButtonModificar.setText("Modificar");
        jButtonModificar.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButtonModificarActionPerformed(evt);
            }
        });

        jButtonMasInfo.setFont(new java.awt.Font("Dialog", 0, 36)); // NOI18N
        jButtonMasInfo.setText("Mas Info");
        jButtonMasInfo.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButtonMasInfoActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout jPanel1Layout = new javax.swing.GroupLayout(jPanel1);
        jPanel1.setLayout(jPanel1Layout);
        jPanel1Layout.setHorizontalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel1Layout.createSequentialGroup()
                .addGap(49, 49, 49)
                .addComponent(jScrollPane1, javax.swing.GroupLayout.PREFERRED_SIZE, 219, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(47, 47, 47)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jButtonEliminar, javax.swing.GroupLayout.PREFERRED_SIZE, 184, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jButtonAgregar, javax.swing.GroupLayout.PREFERRED_SIZE, 184, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jButtonModificar, javax.swing.GroupLayout.PREFERRED_SIZE, 184, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jButtonSalir, javax.swing.GroupLayout.PREFERRED_SIZE, 184, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jButtonMasInfo, javax.swing.GroupLayout.PREFERRED_SIZE, 184, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addContainerGap(45, Short.MAX_VALUE))
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel1Layout.createSequentialGroup()
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addComponent(jLabel1)
                .addGap(196, 196, 196))
        );
        jPanel1Layout.setVerticalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel1Layout.createSequentialGroup()
                .addGap(14, 14, 14)
                .addComponent(jLabel1)
                .addGap(18, 18, 18)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                    .addGroup(jPanel1Layout.createSequentialGroup()
                        .addComponent(jButtonAgregar)
                        .addGap(30, 30, 30)
                        .addComponent(jButtonModificar)
                        .addGap(30, 30, 30)
                        .addComponent(jButtonEliminar)
                        .addGap(30, 30, 30)
                        .addComponent(jButtonMasInfo)
                        .addGap(30, 30, 30)
                        .addComponent(jButtonSalir))
                    .addComponent(jScrollPane1, javax.swing.GroupLayout.PREFERRED_SIZE, 0, Short.MAX_VALUE))
                .addContainerGap(32, Short.MAX_VALUE))
        );

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jPanel1, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jPanel1, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
        );

        pack();
    }// </editor-fold>//GEN-END:initComponents

    private void jButtonSalirActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButtonSalirActionPerformed
        dispose();
        Principal alta = new Principal();
        alta.setVisible(true);
        alta.setLocationRelativeTo(null);
    }//GEN-LAST:event_jButtonSalirActionPerformed

    private void jButtonAgregarActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButtonAgregarActionPerformed
        AgregarSector alta = new AgregarSector(listaSectores);
        alta.setVisible(true);
        alta.setLocationRelativeTo(null); 
        dispose();
    }//GEN-LAST:event_jButtonAgregarActionPerformed

    private void jButtonEliminarActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButtonEliminarActionPerformed
        if(tablaSectores.getRowCount() > 0){
            if(tablaSectores.getSelectedRow()!=-1){
                int resp=JOptionPane.showConfirmDialog(null,"Esta seguro que quiere eliminar el sector " + 
                tablaSectores.getValueAt(tablaSectores.getSelectedRow(), 0).toString() + " ?");
        
                if (JOptionPane.OK_OPTION == resp){
                    nombreSector = tablaSectores.getValueAt(tablaSectores.getSelectedRow(), 0).toString();
            
                    for(Sector dato : listaSectores.getlistaSectores()){
                        if(nombreSector == null ? dato.getNombreSector() == null : nombreSector.equals(dato.getNombreSector())){
                            sectorElim = dato;
                        }
                    }
                    
                    try {
                        listaSectores.eliminarSector(sectorElim.getNombreSector());
                    } catch (SectorInexistenteException ex) {
                        Logger.getLogger(SectorWin.class.getName()).log(Level.SEVERE, null, ex);
                    }
                    
                    dispose();

                    SectorWin alta = new SectorWin(listaSectores);
                    alta.setVisible(true);
                    alta.setLocationRelativeTo(null);
                    }
            }else{
                mostrarMensaje("No seleccion� un registro para eliminar", "Error", "Error al eliminar");
            }
        }else{
                mostrarMensaje("La tabla est� vac�a, no se puede eliminar", "Error", "Error al eliminar");
        }
    }//GEN-LAST:event_jButtonEliminarActionPerformed

    private void jButtonModificarActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButtonModificarActionPerformed
        if(tablaSectores.getRowCount() > 0){
            if(tablaSectores.getSelectedRow()!=-1){
                nombreSector = tablaSectores.getValueAt(tablaSectores.getSelectedRow(), 0).toString();
            
                ModificarSector alta = new ModificarSector(nombreSector, listaSectores);
                alta.setVisible(true);
                alta.setLocationRelativeTo(null);
                
                dispose();
            }else{
                mostrarMensaje("No seleccion� un registro para modificar", "Error", "Error al modificar");
            }
        }else{
                mostrarMensaje("La tabla est� vac�a, no se puede modificar", "Error", "Error al modificar");
        }
    }//GEN-LAST:event_jButtonModificarActionPerformed

    private void jButtonMasInfoActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButtonMasInfoActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_jButtonMasInfoActionPerformed

    private void tablaSectoresMousePressed(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_tablaSectoresMousePressed
        /*
        if (evt.getClickCount()==2) {
            JOptionPane.showMessageDialog(null,jTable1.getValueAt(jTable1.getSelectedRow(),jTable1.getSelectedColumn()).toString());
        }
        */  
    }//GEN-LAST:event_tablaSectoresMousePressed

    public void mostrarMensaje (String mensaje, String tipo, String titulo) { 
        JOptionPane optionPane = new JOptionPane (mensaje);
        if (tipo.equals("Info")) {
            optionPane.setMessageType (JOptionPane. INFORMATION_MESSAGE);
        }else if (tipo.equals("Error")) {
            optionPane.setMessageType(JOptionPane.ERROR_MESSAGE);
            JDialog dialog = optionPane.createDialog(titulo);
            dialog.setAlwaysOnTop(true);
            dialog.setVisible(true);
        }
    }
    
    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton jButton1;
    private javax.swing.JButton jButtonAgregar;
    private javax.swing.JButton jButtonEliminar;
    private javax.swing.JButton jButtonMasInfo;
    private javax.swing.JButton jButtonModificar;
    private javax.swing.JButton jButtonSalir;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JPanel jPanel1;
    private javax.swing.JScrollPane jScrollPane1;
    private javax.swing.JTable tablaSectores;
    // End of variables declaration//GEN-END:variables
}